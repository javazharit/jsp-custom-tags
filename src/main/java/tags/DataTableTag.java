package tags;

import util.Model;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class DataTableTag extends SimpleTagSupport {
    Model model = Model.get();

    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        out.println("<link href=\"css/style.css\" rel=\"stylesheet\" type=\"text/css\"> ");
        out.println("<table id=\"tableParam\" ");
        out.println("<th>");
        out.println(String.format("<td>%s</td><td>%s</td><td>%s</td><td>%s</td>",
                "Submitted Value",
                "Calculated Value",
                "Action",
                "Date"));
        out.println("</th>");

        for (Model.InchvorList hertakanArjeq : model.getArjeqner()) {
            out.println("<tr id=\"trParam\"> ");
            out.print("<td>");
            out.print(hertakanArjeq.getSubmittedValue());
            out.print("</td>");

            out.print("<td>");
            out.print(hertakanArjeq.getCalculatedValue());
            out.print("</td>");

            out.print("<td>");
            out.print(hertakanArjeq.getActionValue());
            out.print("</td>");

            out.print("<td>");
            out.print(hertakanArjeq.getDate());
            out.print("</td>");
            out.println("</tr>");
        }
        out.println("</table>");
    }
}
