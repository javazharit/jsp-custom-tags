package util;

public enum ActionEnum {
    PLUS, MINUS, MULTIPLY, DIVIDE, MOD;

    public static ActionEnum getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }
}
