package util;

import java.util.*;

public class Model {
    private static final int MISSING_PREDECESSOR_VALUE = 100;

    public static class InchvorList {
        private int submittedValue;
        private int calculatedValue;
        private ActionEnum actionValue;
        private Date date;

        public InchvorList(int submittedValue, int calculatedValue, ActionEnum actionValue, Date date) {
            this.submittedValue = submittedValue;
            this.calculatedValue = calculatedValue;
            this.actionValue = actionValue;
            this.date = date;
        }

        public int getSubmittedValue() {
            return submittedValue;
        }

        public int getCalculatedValue() {
            return calculatedValue;
        }

        public ActionEnum getActionValue() {
            return actionValue;
        }

        public Date getDate() {
            return date;
        }
    }

    private List<InchvorList> arjeqner;
    private static Model instance = new Model();

    private Model() {
        arjeqner = new ArrayList<>();
    }

    public static Model get() {
        return instance;
    }

    public String printInchvorArjeq(InchvorList object) {
        StringBuilder sb = new StringBuilder();
        sb.append(object.getSubmittedValue()).append("</br>");
        sb.append(object.getCalculatedValue()).append("</br>");
        sb.append(object.getActionValue()).append("</br>");
        sb.append(object.getDate()).append("</br>");
        return sb.toString();
    }

    public void assignNextValue(int submittedValue, ActionEnum action) {
        int calculatedValue = 0;

        if (arjeqner.size() == 0) {
            calculatedValue = MISSING_PREDECESSOR_VALUE;
        } else {
            InchvorList verjinArjeq = arjeqner.get(arjeqner.size() - 1);

            int lastCalculatedValue = verjinArjeq.getCalculatedValue();
            switch (action) {
                case PLUS:
                    calculatedValue = lastCalculatedValue + submittedValue;
                    break;
                case MINUS:
                    calculatedValue = lastCalculatedValue - submittedValue;
                    break;
                case MULTIPLY:
                    calculatedValue = lastCalculatedValue * submittedValue;
                    break;
                case DIVIDE:
                    calculatedValue = lastCalculatedValue == 0 ?
                            MISSING_PREDECESSOR_VALUE :
                            lastCalculatedValue / submittedValue;
                    break;
                case MOD:
                    calculatedValue = lastCalculatedValue % submittedValue;
                    break;
                default:
                    throw new RuntimeException(String.format("Unknown action type %s", action));
            }
        }

        InchvorList tazaArjeq = new InchvorList(submittedValue, calculatedValue, action, new Date());
        arjeqner.add(tazaArjeq);
    }

    public List<InchvorList> getArjeqner() {
        return arjeqner;
    }

    public InchvorList getArjeq(int index) {
        return arjeqner.get(index);
    }
}
